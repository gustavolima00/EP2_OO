package principal;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.border.EmptyBorder;


public class BatalhaNaval extends JFrame{
	int nLinhas=10;
	int nColunas=10; 
	private final ButtonGroup opcao = new ButtonGroup();
	private JPanel contentPane;
	
	Tabuleiro game = new Tabuleiro(nLinhas, nColunas);
	Jogador jogador = new Jogador(game, 50);
	
	JRadioButton tiroSimples = new JRadioButton("Tiro Simples (1 Ponto)");
	JRadioButton tiroLinha = new JRadioButton("Tiro na Linha/Coluna (5 Ponto)");
	JRadioButton tiroArea = new JRadioButton("Tiro em \u00E1rea (3 Ponto)");
	JRadioButton visao = new JRadioButton("Vis\u00E3o (2 Ponto)");
	JLabel pontos = new JLabel(""+jogador.getRecursos());
	JButton linha[] = new JButton[nColunas];
	GridBagConstraints gbc_linha[] = new GridBagConstraints[nColunas];
	JButton coluna[] = new JButton[nLinhas];
	GridBagConstraints gbc_coluna[] = new GridBagConstraints[nLinhas];
	JButton tabuleiro[][] = new JButton[nLinhas][nColunas];
	GridBagConstraints gbc_tabuleiro[][] = new GridBagConstraints[nLinhas][nColunas];
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BatalhaNaval frame = new BatalhaNaval();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public BatalhaNaval() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 60*nColunas+150, 40*nLinhas);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		//Titulo
		JLabel lblBatalhaNaval = new JLabel("Batalha Naval");
		lblBatalhaNaval.setFont(new Font("Trebuchet MS", Font.BOLD, 20));
		GridBagConstraints gbc_lblBatalhaNaval = new GridBagConstraints();
		gbc_lblBatalhaNaval.gridwidth = 2;
		gbc_lblBatalhaNaval.insets = new Insets(0, 0, 5, 5);
		gbc_lblBatalhaNaval.gridx = 0;
		gbc_lblBatalhaNaval.gridy = 0;
		contentPane.add(lblBatalhaNaval, gbc_lblBatalhaNaval);
		
		opcao.add(tiroSimples);
		tiroSimples.setFont(new Font("Trebuchet MS", Font.BOLD, 10));
		GridBagConstraints gbc_tiroSimples = new GridBagConstraints();
		gbc_tiroSimples.gridwidth = 2;
		gbc_tiroSimples.anchor = GridBagConstraints.WEST;
		gbc_tiroSimples.insets = new Insets(0, 0, 5, 5);
		gbc_tiroSimples.gridx = 0;
		gbc_tiroSimples.gridy = 1;
		contentPane.add(tiroSimples, gbc_tiroSimples);
		

		opcao.add(tiroArea);
		tiroArea.setFont(new Font("Trebuchet MS", Font.BOLD, 10));
		GridBagConstraints gbc_tiroArea = new GridBagConstraints();
		gbc_tiroArea.gridwidth = 2;
		gbc_tiroArea.anchor = GridBagConstraints.WEST;
		gbc_tiroArea.insets = new Insets(0, 0, 5, 5);
		gbc_tiroArea.gridx = 0;
		gbc_tiroArea.gridy = 2;
		contentPane.add(tiroArea, gbc_tiroArea);
		
		opcao.add(visao);
		visao.setFont(new Font("Trebuchet MS", Font.BOLD, 10));
		GridBagConstraints gbc_visao = new GridBagConstraints();
		gbc_visao.gridwidth = 2;
		gbc_visao.anchor = GridBagConstraints.WEST;
		gbc_visao.insets = new Insets(0, 0, 5, 5);
		gbc_visao.gridx = 0;
		gbc_visao.gridy = 3;
		contentPane.add(visao, gbc_visao);
		
		opcao.add(tiroLinha);
		tiroLinha.setFont(new Font("Trebuchet MS", Font.BOLD, 10));
		GridBagConstraints gbc_tiroLinha = new GridBagConstraints();
		gbc_tiroLinha.gridwidth = 2;
		gbc_tiroLinha.anchor = GridBagConstraints.WEST;
		gbc_tiroLinha.insets = new Insets(0, 0, 5, 5);
		gbc_tiroLinha.gridx = 0;
		gbc_tiroLinha.gridy = 4;
		contentPane.add(tiroLinha, gbc_tiroLinha);
		
		for (int i=0; i<nLinhas; i++) {
			int letra='A'+i;
			linha[i] = new JButton(""+(char)letra);
			linha[i].setFont(new Font("Trebuchet MS", Font.BOLD, 12));
			linha[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evento){
					jogar((JButton) evento.getSource());
				}
				});
			gbc_linha[i] = new GridBagConstraints();
			gbc_linha[i].insets = new Insets(0, 0, 5, 5);
			gbc_linha[i].gridy = i;
			gbc_linha[i].gridx = 2;
			contentPane.add(linha[i], gbc_linha[i]);
			
			for(int j=0; j<nColunas; j++) {
				tabuleiro[i][j] = new JButton(" ");
				tabuleiro[i][j].setFont(new Font("Trebuchet MS", Font.BOLD, 12));
				tabuleiro[i][j].addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent evento){
						jogar((JButton) evento.getSource());
					}
					});
				gbc_tabuleiro[i][j] = new GridBagConstraints();
				gbc_tabuleiro[i][j].insets = new Insets(0, 0, 5, 5);
				gbc_tabuleiro[i][j].gridy = i;
				gbc_tabuleiro[i][j].gridx = 3+j;
				contentPane.add(tabuleiro[i][j], gbc_tabuleiro[i][j]);		
			}
		}
		
		for(int i=0; i<nColunas; i++) {
			coluna[i] = new JButton(""+(i+1));
			coluna[i].setFont(new Font("Trebuchet MS", Font.BOLD, 12));
			coluna[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent evento){
					jogar((JButton) evento.getSource());
				}
				});
			gbc_coluna[i] = new GridBagConstraints();
			gbc_coluna[i].insets = new Insets(0, 0, 5, 5);
			gbc_coluna[i].gridy = nLinhas+1;
			gbc_coluna[i].gridx = 3+i;
			contentPane.add(coluna[i], gbc_coluna[i]);
		}
		
		int recursos=jogador.getRecursos();
		
		JLabel lblNewLabel = new JLabel("Pontos:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 5;
		contentPane.add(lblNewLabel, gbc_lblNewLabel);
		
		
		pontos.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_pontos = new GridBagConstraints();
		gbc_pontos.insets = new Insets(0, 0, 5, 5);
		gbc_pontos.gridx = 1;
		gbc_pontos.gridy = 5;
		contentPane.add(pontos, gbc_pontos);
		
		JLabel legenda = new JLabel("Legenda");
		legenda.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_legenda = new GridBagConstraints();
		gbc_legenda.gridwidth = 2;
		gbc_legenda.insets = new Insets(0, 0, 5, 5);
		gbc_legenda.gridx = 0;
		gbc_legenda.gridy = 6;
		contentPane.add(legenda, gbc_legenda);
		
		JLabel legenda1 = new JLabel("[ * ] Tiro na agua ");
		legenda1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_legenda1 = new GridBagConstraints();
		gbc_legenda1.gridwidth = 2;
		gbc_legenda1.insets = new Insets(0, 0, 5, 5);
		gbc_legenda1.gridx = 0;
		gbc_legenda1.gridy = 7;
		contentPane.add(legenda1, gbc_legenda1);
		
		JLabel legenda2 = new JLabel(" [ # ] Barco Atingido ");
		legenda2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		GridBagConstraints gbc_legenda2 = new GridBagConstraints();
		gbc_legenda2.gridwidth = 2;
		gbc_legenda2.insets = new Insets(0, 0, 5, 5);
		gbc_legenda2.gridx = 0;
		gbc_legenda2.gridy = 8;
		contentPane.add(legenda2, gbc_legenda2);
		Atualizar();
	}
	
	public void Atualizar() {
		int recursos=jogador.getRecursos();
		pontos.setText(""+recursos);
		for (int i=0; i<nLinhas; i++) {
			for(int j=0; j<nColunas; j++) {
				int valor=game.getTabuleiro(i, j);
				String conteudo;
				if(valor==Tabuleiro.AGUA || valor==Tabuleiro.BARCO){
					conteudo=" ";
				}
				else if(valor==Tabuleiro.TIRO_AGUA){
					conteudo="*";
				}
				else if(valor==Tabuleiro.TIRO_BARCO){
					conteudo="#";
				}
				else{
					conteudo="erro!!";
				}
				tabuleiro[i][j].setText(conteudo);
			}
		}
		int status=jogador.Status();
		if(status==Jogador.VITORIA){
			JOptionPane.showMessageDialog(null, "Parabens!! Voce venceu");
			System.exit(0);
		}
		else if(status==Jogador.DERROTA){
			JOptionPane.showMessageDialog(null, "Seus pontos acabaram. Voce perdeu");
			System.exit(0);
		}
	}
	
	public void jogar(JButton botao){
		
		for(int i=0; i<nLinhas; i++){
			if(botao == linha[i]) {
				if(!(tiroLinha.isSelected())) {
					JOptionPane.showMessageDialog(null, "Para atacar uma linha inteira, selecione a opcao \"Tiro na Linha/Coluna\"");
				}
				else {
					int resultado = jogador.AtaqueLinha(i);
					if(resultado==Jogador.SEM_RECURSOS){
						JOptionPane.showMessageDialog(null, "Recursos Insuficientes");
					}
					else if(!(resultado==Jogador.SUCESSO)){
						JOptionPane.showMessageDialog(null, "Algo de errado aconteceu :(");
					}
				}
				Atualizar();
				return;
			}
			else if(botao == coluna[i]) {
				if(!(tiroLinha.isSelected())) {
					JOptionPane.showMessageDialog(null, "Para atacar uma linha inteira, selecione a opcao \"Tiro na Linha/Coluna\"");
				}
				else {
					int resultado =jogador.AtaqueColuna(i);
					if(resultado==Jogador.SEM_RECURSOS){
						JOptionPane.showMessageDialog(null, "Recursos Insuficientes");
					}
					else if(!(resultado==Jogador.SUCESSO)){
						JOptionPane.showMessageDialog(null, "Algo de errado aconteceu :(");
					}
				}
				Atualizar();
				return;
			}
		}
		for(int i=0; i<nLinhas; i++){
			for(int j=0; j<nColunas; j++){
				if(botao == tabuleiro[i][j]){
					if(tiroLinha.isSelected()) {
						JOptionPane.showMessageDialog(null, "Selecione uma linha ou uma coluna");
					}
					else if(tiroSimples.isSelected()) {
						int resultado=jogador.AtaqueSimples(i, j);
						if(resultado==Jogador.SEM_RECURSOS){
							JOptionPane.showMessageDialog(null, "Recursos Insuficientes");
						}
						else if(resultado==Jogador.BARCO_ATACADO){
							JOptionPane.showMessageDialog(null, "Esse barco já foi atacado");
						}
						else if(!(resultado==Jogador.SUCESSO)){
							JOptionPane.showMessageDialog(null, "Algo de errado aconteceu :(");
						}
					}
					else if(tiroArea.isSelected()) {
						int resultado=jogador.AtaqueArea(i, j);
						if(resultado==Jogador.SEM_RECURSOS){
							JOptionPane.showMessageDialog(null, "Recursos Insuficientes");
						}
						else if(resultado==Jogador.AREA_ATACADA){
							JOptionPane.showMessageDialog(null, "Essa area ja foi atacada");
						}
						else if(resultado==Jogador.CASA_INVALIDA){
							JOptionPane.showMessageDialog(null, "Selecao invalida");
						}
						else if(!(resultado==Jogador.SUCESSO)){
							JOptionPane.showMessageDialog(null, "Algo de errado aconteceu :(");
						}
					}
					else if(visao.isSelected()) {
						int resultado=jogador.Visao(i, j);
						if(resultado==Jogador.SEM_RECURSOS){
							JOptionPane.showMessageDialog(null, "Recursos Insuficientes");
						}
						else if(resultado==Jogador.NAO_TEM_BARCO) {
							JOptionPane.showMessageDialog(null, "Nao possui barco nesta area");
						}
						else if(resultado==Jogador.TEM_BARCO) {
							JOptionPane.showMessageDialog(null, "Possui alguma embarcacao nessa area!!");
						}
						else {
							JOptionPane.showMessageDialog(null, "Algo de errado aconteceu :(");
						}
					}
					else {
						JOptionPane.showMessageDialog(null, "Selecione uma opcao");
					}
					Atualizar();
					return;
				}
			}
		}	
	}
}