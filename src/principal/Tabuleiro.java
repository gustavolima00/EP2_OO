package principal;

import java.util.Random;

public class Tabuleiro {
	private int largura;
	private int altura;
	private int[][] tabuleiro;
	
	public static final int INVALIDO=0;
	public static final int AGUA=1;
	public static final int TIRO_AGUA=2;
	public static final int BARCO=3;
	public static final int TIRO_BARCO=4;
	public static final int SUBMARINO=5;
	public static final int PORTA_AVIAO=6;
	public static final int CRUZADOR=7;
	public static final int ENCOURACADO=8;

	public Tabuleiro(int largura, int altura){
		this.largura=largura;
		this.altura=altura;
		this.tabuleiro=new int[largura][altura];
		for(int i=0; i<altura; i++)
			for(int j=0; j<largura; j++)
				this.tabuleiro[i][j]=AGUA;
		PosicionarBarcos();
			
	}
	public static final int N_SUBMARINOS=3;
	public static final int N_PORTA_AVIOES=1;
	public static final int N_CRUZADORES=3;
	public static final int N_ENCOURACADOS=2;
	
	public void PosicionarBarcos(){
		Random gerador = new Random();
		boolean aux;
		int i, linha, coluna, posicao;
		if(largura<7 || altura<7){
			System.out.println("Dimensões muito pequenas");
			return;
		}
		//Posicionamento dos subarinos
		i=0;
		while(i<N_SUBMARINOS){
			linha=gerador.nextInt(this.altura);
			coluna=gerador.nextInt(this.largura);
			if(getTabuleiro(linha,coluna)==AGUA){
				setTabuleiro(linha, coluna, SUBMARINO);
				i++;
			}
		}
		
		//Posicionamento dos porta avioes
		i=0;
		while(i<N_PORTA_AVIOES){
			posicao=gerador.nextInt(2);
			//Barco ficará deitado
			if(posicao==0){
				linha=gerador.nextInt(this.altura);
				coluna=gerador.nextInt(this.largura-5);
				aux=true;
				for(int j=0; j<5; j++){
					if(getTabuleiro(linha, j)!=AGUA){
						aux=false;
						break;
					}
				}
				if(aux){
					for(int j=0; j<5; j++)
						setTabuleiro(linha, j, PORTA_AVIAO);
					i++;
				}
			}
			else{
				linha=gerador.nextInt(this.altura-5);
				coluna=gerador.nextInt(this.largura);
				aux=true;
				for(int j=0; j<5; j++){
					if(getTabuleiro(j, coluna)!=AGUA){
						aux=false;
						break;
					}
				}
				if(aux){
					for(int j=0; j<5; j++)
						setTabuleiro(j, coluna, PORTA_AVIAO);
					i++;
				}
			}
		}
		
		//Posicionamento dos cruzadores
		i=0;
		while(i<N_CRUZADORES){
			posicao=gerador.nextInt(2);
			//Barco ficará deitado
			if(posicao==0){
				linha=gerador.nextInt(this.altura);
				coluna=gerador.nextInt(this.largura-2);
				aux=true;
				for(int j=0; j<2; j++){
					if(getTabuleiro(linha, j)!=AGUA){
						aux=false;
						break;
					}
				}
				if(aux){
					for(int j=0; j<2; j++)
						setTabuleiro(linha, j, CRUZADOR);
					i++;
				}
			}
			else{
				linha=gerador.nextInt(this.altura-2);
				coluna=gerador.nextInt(this.largura);
				aux=true;
				for(int j=0; j<2; j++){
					if(getTabuleiro(j, coluna)!=AGUA){
						aux=false;
						break;
					}
				}
				if(aux){
					for(int j=0; j<2; j++)
						setTabuleiro(j, coluna, CRUZADOR);
					i++;
				}
			}
		}
		
		//Posicionamento dos encouracados
		i=0;
		while(i<N_ENCOURACADOS){
			posicao=gerador.nextInt(2);
			//Barco ficará deitado
			if(posicao==0){
				linha=gerador.nextInt(this.altura);
				coluna=gerador.nextInt(this.largura-4);
				aux=true;
				for(int j=0; j<4; j++){
					if(getTabuleiro(linha, j)!=AGUA){
						aux=false;
						break;
					}
				}
				if(aux){
					for(int j=0; j<4; j++)
						setTabuleiro(linha, j, ENCOURACADO);
					i++;
				}
			}
			else{
				linha=gerador.nextInt(this.altura-4);
				coluna=gerador.nextInt(this.largura);
				aux=true;
				for(int j=0; j<4; j++){
					if(getTabuleiro(j, coluna)!=AGUA){
						aux=false;
						break;
					}
				}
				if(aux){
					for(int j=0; j<4; j++)
						setTabuleiro(j, coluna, ENCOURACADO);
					i++;
				}
			}
		}
		
	}
	
	public int getLargura() {
		return largura;
	}
	
	public int getAltura() {
		return altura;
	}
	
	public int getTabuleiro(int linha, int coluna){
		if(coluna>this.largura || linha>this.altura){
			return INVALIDO;
		}
		
		int temp=this.tabuleiro[linha][coluna];
		if(temp==AGUA || temp==TIRO_AGUA || temp==TIRO_BARCO){
			return temp;
		}
		else if(temp==SUBMARINO || temp==PORTA_AVIAO || temp==CRUZADOR || temp==ENCOURACADO){
			return BARCO;
		}
		else{
			return INVALIDO;
		}
	}
	
	//Caso a função tenha algum algumento ela retorna valores reais da matriz
	public int getTabuleiro(int linha, int coluna, char arg){
		if(coluna>this.largura || linha>this.altura){
			return INVALIDO;
		}
		else return this.tabuleiro[linha][coluna];
	}
	
	public boolean setTabuleiro(int linha, int coluna, int valor){
		if(valor!=AGUA && valor!=TIRO_AGUA && valor!=BARCO && valor!=TIRO_BARCO && valor!=SUBMARINO && valor!=PORTA_AVIAO && valor!=CRUZADOR && valor!=ENCOURACADO ){
			System.out.println("Valor inválido");
			return false;
		}
		this.tabuleiro[linha][coluna]=valor;
		return true;
	}
}
